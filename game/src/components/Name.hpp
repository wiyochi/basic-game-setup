#pragma once

#include <string>

namespace components
{
	using Name = std::string;
}