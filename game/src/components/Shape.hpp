#pragma once

#include <SFML/Graphics.hpp>

#include <components/Position.hpp>
#include <utils/Vec2.hpp>

namespace components
{
	class Shape : public sf::Drawable
	{
	public:
		Shape(components::Position const& pos, Vec2 const& size);
		~Shape();

		void showDebug(bool value = true);

		void setSize(Vec2 const& size) { _rectShape.setSize(sf::Vector2f(size.x - 2, size.y - 2)); }
		void setPosition(Vec2 const& pos) { _rectShape.setPosition(sf::Vector2f(pos.x + 1, pos.y + 1)); }
		void setRotation(float angle) { _rectShape.setRotation(angle); }

		Vec2 getSize() const { return _rectShape.getSize(); };
		Vec2 getPosition() const { return _rectShape.getPosition(); }

	private:
		sf::RectangleShape _rectShape;

		bool _debugOn;

	private:
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	};
}