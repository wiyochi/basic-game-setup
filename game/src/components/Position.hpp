#pragma once

#include <utils/Vec2.hpp>

namespace components
{
	using Position = Vec2;
}