#include <game/GameObject.hpp>

#include <components/Name.hpp>

namespace game
{
	GameObject::GameObject(std::string const& name, std::shared_ptr<World> world) :
		_world(world),
		_id(_world->createGameObject()),
		_nameComponent(addComponent<components::Name>(name))
	{
	}

	GameObject::~GameObject()
	{
	}
}