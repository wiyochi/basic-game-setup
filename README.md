# Basic Game Setup
Projet de centralisation de librairie c++:
 - [SFML](https://www.sfml-dev.org/ "Simple and Fast Multimedia Library")
 - [Box2D](https://box2d.org/ "A 2D Physics Engine for Games")
 - [spdlog](https://github.com/gabime/spdlog "Very fast, header-only/compiled, C++ logging library")
 - [entt](https://github.com/skypjack/entt "ECS in modern C++")
 - [nlohmann](https://github.com/nlohmann/json "JSON library")

---
Mathieu Arquillière - Jérémy Zangla - Octobre 2020